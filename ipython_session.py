import logging
from logging import handlers
import sys

import sqlalchemy

import utils
from utils.godb_models import *
from utils.godb_session import godb_session_maker

import pachi
from config import DB_FILE

"""Just a utility file to save imports"""

if __name__ == '__main__':
    s = godb_session_maker(filename=DB_FILE)
