from os.path import abspath, exists
import os

from config import OUTPUT_DIR

from utils.colors import * 
from utils import misc

"""Utility class to generate a randomly named (without collisions)
file to output i.e. pattern files from pachi. """

class ResultFile:
    def __init__(self, filename, create_empty=False):
        self.filename = filename
        if create_empty:
            assert not self.exists()
            open(self.filename,'w').close()
            
    def exists(self, warn=False):
        status = exists(self.filename)
        if not status and warn:
            logging.warn("File '%s' does not exist."%(self.filename,))
        return status
            
    def __repr__(self):
        return "ResultFile('%s')"%(self.filename,)

def get_random_output_base(sub_len=3, levels=2):
    h = misc.unique_hash()
    assert len(h) > levels * sub_len
    assert levels >= 1
    l = [OUTPUT_DIR]
    for x in xrange(levels):
        l.append( h[ x * sub_len : (x+1) * sub_len ] )
        
    d = os.path.join(*l)
    if not os.path.isdir(d):
        os.makedirs(d)
    return os.path.join(d, h)
    
def get_output_resultfile(suffix=''):
    ret = ResultFile( get_random_output_base() + suffix)
    if ret.exists():
        raise RuntimeError("New output result file '%s' already exists, unique hash not really unique..."%(ret))
    return ret

def get_output_resultpair(suffix=''):
    basename = get_random_output_base()
    ret1 = ResultFile(basename + '_B' + suffix)
    ret2 = ResultFile(basename + '_W' + suffix)
    rettup = BlackWhite(ret1, ret2)
    for ret in rettup:
        if ret.exists():
            raise RuntimeError("New output result file '%s' already exists, unique hash not really unique..."%(ret))
    return rettup




