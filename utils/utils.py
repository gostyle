import re
import subprocess
import os
import copy
from os.path import abspath, exists
import shutil
import functools
import inspect
import itertools
import numpy

import misc
from colors import BlackWhite
import types

VIEWER_LIST=['qgo', 'kombilo']

def viewer_open(sgf_filename, executable=VIEWER_LIST[1]):
    p = subprocess.Popen([executable, sgf_filename])
    return

def bark():
    subprocess.call('bark', shell=True)

def check_output(*args,  **kwargs):
    if hasattr(subprocess, 'check_output'):
        return subprocess.check_output(*args, **kwargs)
    else:
        if 'stdout' in kwargs:
            raise ValueError('stdout argument not allowed, it will be overridden.')
        process = subprocess.Popen(stdout=subprocess.PIPE, *args, **kwargs)
        output, unused_err = process.communicate()
        retcode = process.poll()
        if retcode:
            cmd = kwargs.get("args")
            if cmd is None:
                cmd = args[0]
            raise subprocess.CalledProcessError(retcode, cmd, output=output)
        return output

def get_year(datestr,  match_century=True):
    """Function trying to extract date from a string - usually a DT field of a sgf_file.
    First, look for the first year string in the @datestr.
    if not found and the @match_century is true, we look for strings like
    "18th century", which results in year 1750 (the mean of years in 18th century)
    
    Returns None if no year found.
    """
    # 1982-10-10
    # note the non-greedy .*? expansion => the first date string in the result gets matched
    # that is get_year("1982 1999") = 1982
    match = re.match( '.*?([0-9]{4}).*', datestr)
    if match:
            return int(match.group(1))
        
    if match_century:
        # 17th century, 18th c.
        match = re.match( '.*[^0-9]?([0-9]{2}th c).*', datestr)
        if match:
            century = int(match.group(1)[:2])
            # returns "mean" year of the century:
            # 17th century -> 1650
            return century * 100 - 50
    
    return None

def get_poly_s(coefs):
    """Returns a string with polynomial equation; e.g.:
    
    >>> from utils import get_poly_s
    >>> get_poly_s([0.5,0,4])
    'y = 0.50x^2 + 4.00'
    >>> get_poly_s([1,2,3,4])
    'y = 1.00x^3 + 2.00x^2 + 3.00x + 4.00'
    """
    C = []
    for pw, co in enumerate(reversed(coefs)):
        if co:
            s = "%.2f" % co
            if pw:
                s += 'x'
                if pw > 1:
                    s += '^%d' % pw
            C.append(s)
    return 'y = ' + ' + '.join(reversed(C))


"""

class ReprWrapper(object):
    def __init__(self, repr_f, f):
        self.repr_f = repr_f
        self.f = f
        functools.update_wrapper(self, f)
    def __call__(self, *args, **kwargs):
        return self.f(*args, **kwargs)
    def __repr__(self):
        return self.repr_f(self.f)
"""

def repr_origin(f):
    if hasattr(f, 'im_class'):
        prefix = f.im_class
    else:
        prefix = f.__module__
    return "%s.%s"%(prefix, f.__name__)

def head(f, n=10):
    print f.filename
    with open(f.filename, 'r') as fin:
        for line in itertools.islice(fin, n):
            print line

def iter_splits(l, parts=None, max_size=None, min_size=None):
    """Will yield consequent sublist of the @l list, trying to result
    evenly sized sublists.  Exactly one of the parameters @parts or
    @max_size or @min_size must be specified.

    specifiing parts = N will yield N sublists of (almost) even size. The
    list size difference is guaranted to be at most 1.

    >>> list(iter_splits(range(5), parts=2))
    [[0, 1, 2], [3, 4]]
    >>> list(iter_splits(range(5), parts=4))
    [[0, 1], [2], [3], [4]]
    
    
    
    specifiing max_size = N returns the smallest possible number of
    consequent sublists so that whole list is divided and size of each
    part is <= N
    
    >>> list(iter_splits(range(5), max_size=3))
    [[0, 1, 2], [3, 4]]
    >>> list(iter_splits(range(5), max_size=10))
    [[0, 1, 2, 3, 4]]

    Calling iter_splits(l, max_size=N) is just a shorthand for calling
    iter_splits(l, parts=len(l) / N + bool(len(l)% N) )
    
    
    
    Similarly min_size = N returns the largest possible number of
    consequent sublists so that whole list is divided and size of each
    part is >= N
    
    Calling iter_splits(l, min_size=N) is just a shorthand for calling
    iter_splits(l, parts=len(l) / N )
    """
    if bool(parts) + bool(max_size) + bool( min_size) != 1:
        raise TypeError('Exactly one of parts, max_size or exact_size arguments must be specified (and nonzero)')
    
    if parts:
        print parts
        pn, rest = len(l) / parts, len(l) % parts
        if pn == 0:
            raise ValueError("Number of parts to split must not be larger than the number of elements.")
        
        def sizes(pn, rest):
            for i in xrange(parts):
                if rest:
                    yield pn + 1
                    rest -= 1
                else:
                    yield pn
        
        stop = 0
        for size in sizes(pn, rest):
            start, stop = stop, stop + size
            yield l[start: stop]
            
    if max_size:
        pn, rest = len(l) / max_size, len(l) % max_size
        if rest:
            pn += 1
        for split in iter_splits(l, parts=pn):
            yield split
            
    if min_size:
        for split in iter_splits(l, parts=len(l)/min_size):
            yield split
        
def iter_exact_splits(l, split_size):
    tail = copy.copy(l)
    
    while tail:
        head, tail = tail[:split_size], tail[split_size:]
        # the last head could be shorter
        if len(head) == split_size:
            yield head
            
def pearson_coef(vec1, vec2):
    assert vec1.shape == vec2.shape
    def norm(vec):
        return numpy.sqrt((vec*vec).sum())
    def center(vec):
        return vec - vec.mean()
    vec1, vec2 = center(vec1), center(vec2)
    return (vec1 * vec2).sum() / (norm(vec1) * norm(vec2)) 
    
            
if __name__ == '__main__':
    def test_split():
        l = range(20)
        
        for kw in ['parts', 'max_size',  'min_size']:
            for val in  range(10, 20):
                print "iter_splits(%s, **{%s : %s}))" % (l,  kw,  val)
                res = list(iter_splits(l, **{kw : val}))
                print kw, "=", val
                print "   len = ", len(res), ", max(size) = ", max(map(len, res)), ", min(size) = ", min(map(len, res))
                print "   ", res
                
                assert list(itertools.chain.from_iterable(res)) == l
                if kw == 'parts':
                    assert len(res) == val
                if kw == 'max_size':
                    assert max(map(len, res)) <= val
                if kw == 'min_size':
                    assert min(map(len, res)) >= val
                
    #test_partial()
    #test_split()    
    
    get_random_output_base(0, 1)