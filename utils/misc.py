#coding: utf-8
import unicodedata
from itertools import chain, izip,  count
import hashlib
import random
import logging
import threading
import os
import time
import datetime

def identity(x):
    return x

#
# Functional tools
#

def filter_null(iterable):
    return [ x for x in iterable if x ]

def filter_both(predicate, iterable):
    yes, no = [], []
    for i in iterable:
        if predicate(i):
            yes.append(i)
        else:
            no.append(i)
    return yes, no

def flatten(list_of_lists):
    return chain.from_iterable(list_of_lists)

def flatten_twice(list_of_lists_of_lists):
    return flatten(flatten( list_of_lists_of_lists ))

def argmax(pairs):
    """Given an iterable of pairs (key, value), return the key corresponding to the greatest value."""
    return max(pairs, key=lambda x:x[1])[0]

def argmin(pairs):
    return min(pairs, key=lambda x:x[1])[0]

def argmax_index(values):
    """Given an iterable of values, return the index of the greatest value."""
    return argmax(izip(count(), values))

def argmin_index(values):
    return argmin(izip(count(), values))

def bucket_by_key(iterable, key_fc):
    """
    Throws items in @iterable into buckets given by @key_fc function.
    e.g.
    >>> bucket_by_key([1,2,-3,4,5,6,-7,8,-9], lambda num: 'neg' if num < 0 else 'nonneg')
    {'neg': [-3, -7, -9], 'nonneg': [1, 2, 4, 5, 6, 8]}
    """
    buckets = {}
    for item in iterable:
        buckets.setdefault(key_fc(item), []).append(item)
    return buckets

def first_true_pred(predicates, value):
    """Given a list of predicates and a value, return the index of first predicate,
    s.t. predicate(value) == True. If no such predicate found, raises IndexError."""
    for num, pred in enumerate(predicates):
        if pred(value):
            return num
    raise IndexError

#
# Partial 
#

class MyPartial:
    """
    An alternative implementation of functools partial, allowing to specify args
    from the right as well.
    """
    def __init__(self, func, args=(), keywords={}, right=False):
        self.func = func
        self.args = args
        self.keywords = keywords
        self.right = right
        
    def _frepr(self):
        return repr(self.func)
        
    def __repr__(self):
        return "MyPartial(%s, %s, %s%s)"%(self._frepr(),
                                          repr(self.args), repr(self.keywords),
                                          ", right=True" if self.right else '')
    
    def _merge_args(self, args_new):
        if self.right:
            return args_new + tuple(self.args)
        return tuple(self.args) + args_new
    
    def _merge_kwargs(self, kwargs_new):
        kwargs = self.keywords.copy()
        kwargs.update(kwargs_new)
        return kwargs
    
    def __call__(self, *args_new, **kwargs_new):
        args = self._merge_args(args_new)
        kwargs = self._merge_kwargs(kwargs_new)
        return self.func(*args, **kwargs)
    
def partial(f, *args, **kwargs):
    """
    def minus(a, b):
        return a - b
    
    partial(minus, 10)  is like:
    
    lambda b : minus(10, b)
    """
    return MyPartial(f, args, kwargs)

def partial_right(f, *args, **kwargs):
    """
    def minus(a, b):
        return a - b
    
    partial_right(minus, 10)  is like:
    
    lambda a : minus(a, 10)
    """
    return MyPartial(f, args, kwargs, right=True)

#
# Type info and conversion
#

def is_conv(x, cls):
    try:
        cls(x)
        return True
    except:
        return False

def is_int(x):
    return is_conv(x, int)

def is_float(x):
    return is_conv(x, float)

def is_type(x, type):
    try:
        return x == type(x)
    except:
        return False

def is_type_int(x):
    return is_type(x, int)

def is_type_float(x):
    return is_type(x, float)

#
# Hash utils & random strings
#

def sha256(txt):
    return hashlib.sha256(txt).hexdigest()
def sha512(txt):
    return hashlib.sha512(txt).hexdigest()

def random_hash(LEN=10):
    return str(random.randint(10**(LEN-1),10**LEN-1))

def unique_hash(length=32):
    """Returns "unique" hash. (the shorter the length, the less unique it is).
    I consider one in 16**32 to be pretty unique. :-) (supposing that sha256 works).
    
    (Am I mistaken??)"""
    return sha256( "%.20f %s %d %d"%(time.time(), random_hash(), os.getpid(), threading.current_thread().ident ) ) [:length]

time_based_random_hash =  unique_hash

def tmp_names(base=random_hash(), first_simple=False):
    i = 0
    if first_simple:
        yield "%s"%(base)
        i += 1
    while True:
        yield "%s_%d"%(base, i)
        i+=1

#
# Text stuff
#

def encode_utf8(st):
    return unicode(st).encode('utf-8')

def remove_accents(istr):
    nkfd_form = unicodedata.normalize('NFKD', unicode(istr))
    return u"".join([c for c in nkfd_form if not unicodedata.combining(c)])

def unicode2ascii(unistr):
    return remove_accents(unistr).encode('ascii', 'ignore')

def pad2len(s, l, padchar):
    if len(s) < l:
        return s + padchar * (l - len(s)) 
    return s

#
#
#

def iter_files(directory):
    for (dirpath, _, filenames) in os.walk(directory):
        for name in filenames:
                yield os.path.join(dirpath, name)

def timeit(f, *args, **kwargs):
    t0 = time.time()
    ret = f(*args, **kwargs)
    diff = time.time() - t0
    print "TOOK: %.3f sec"%(diff,)
    return ret

if __name__ == '__main__':
    def test_partial():
        def fc ( a, b, c='def C', d='def D' ):
            return "a=%d, b=%d, c=%s, d=%s"%(a,b,c,d)
        
        nor = partial(fc, 20, c=10)
        zpr = partial_right(fc, 20, c=10)
        
        print "puvodni:", repr(fc)
        print "normalni:", repr(nor) 
        print "zprava:", repr(zpr) 
        
        print "normalni(10) = ", nor(10)
        print "zprava(10) = ", zpr(10)
            
        nor2 = partial(nor, 10)    
        print "double:", nor2
        print "double()", nor2()
        
        class Fobj:
            def __call__(self, a, b):
                return "a=%s b=%s"%(a,b)
        
        ca = partial(Fobj(), 10)
        print ca
